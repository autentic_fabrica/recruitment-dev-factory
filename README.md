# Recruitment-Dev-Factory - AutenTIC

## Game - Poker

### Requerimiento
Construir una aplicación web para una persona basado en el famoso juego de **Poker** utilizando las tecnologías con las que te sientas mas cómodo.

### Aclaraciones
Para el correcto desarrollo de la prueba, es necesario que tengas en cuenta los siguientes puntos:
+ Ten en cuenta que la aplicación será ejecutada en otras máquinas. Asegúrate de no tener referencias locales.
+ No es necesario que te preocupes por las cartas. Encontraras aquí imagenes con las cartas disponibles.

### Bonus
+ Puedes utilizar todas las tecnologías que veas conveniente para cumplir con el requerimiento. No obstante, la utilización de algunas en particular darán puntos adicionales a la hora de la revisión del código:
   + MCV | WebApi | SpringBoot.
   + Angular | AngularJS | ReactJS | Android | IOS.
   + Bootstrap.
+ Código limpio.
+ Seguir principio SOLID.
+ Codificación en inglés.
+ Utilizar un repositorio en github.

### Mecánicas
+ La aplicación al iniciar mostrará 5 cartas al azar y un resumen de las cartas listadas.
+ La aplicación tendrá un botón para volver a repartir las cartas y el resumen deberá ser actualizado.
   + Cuantos pares?
   + Cuantos tríos?
   + Hay escalera?
   + Hay póker?
   + Hay Color?

#### (Opcional, solo si separas la aplicación en Backend y Frontend)
+ La selección aleatoria será en el lado del servidor.
+ El análisis/resumen de las cartas expuestas debe ser en el cliente (JS).

### Screen Shoot ###

![picture alt](./pokergame.png)

#### Info
+ juan.norena@autentic.com.co